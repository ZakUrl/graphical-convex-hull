from kivy.graphics import Ellipse
from kivy.uix.image import Image
from random import choice


PLANETS = ["./img/earth.png","./img/jupiter.png","./img/mars.png","./img/mercury.png","./img/moon.png","./img/saturn.png","./img/sun.png","./img/venus.png"]
EXPLOSION = "./img/explosion.png"

class Planet(Ellipse):

    SIZE = 100

    def __init__(self,coordinates,**kwargs):
        super(Planet,self).__init__(**kwargs)
        self.pos = coordinates
        self.size = (Planet.SIZE,Planet.SIZE)
        self.texture = Image(source=choice(PLANETS)).texture

    def getX(self):
        return self.pos[0]

    def getY(self):
        return self.pos[1]

    def getSize(self):
        return SIZE

    def destroy(self):
        self.size = (Planet.SIZE*(3),Planet.SIZE*(1.5))
        self.texture = Image(source=EXPLOSION).texture

    def __str__(self):
        return f"P({self.pos[0]},{self.pos[1]})"

    def __repr__(self):
        return f"P({self.pos[0]},{self.pos[1]})"