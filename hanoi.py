import pygame
import random
import sys


def main():
    pygame.init()
    pygame.display.set_caption('hanoi')
    screen = pygame.display.set_mode((816, 624))
    background = pygame.image.load("img/background.png")
    tower_map = TowerMap()
    iterator = tower_map.solve_hanoi(tower_map.num_disks())
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    next_iterator(iterator)
                if event.key == pygame.K_ESCAPE:
                    iterator = reset(tower_map, iterator)
                if event.key == pygame.K_UP:
                    iterator = add_disk(tower_map, iterator)
                if event.key == pygame.K_DOWN:
                    iterator = remove_disk(tower_map, iterator)
        screen.fill((0, 0, 0))
        screen.blit(background, (0, 0))
        screen.blit(tower_map.image, (0, 0))
        for disk in tower_map.disks():
            screen.blit(disk.image, disk.rect)
        pygame.display.update()

def next_iterator(iterator):
    try:
        next(iterator)
    except StopIteration:
        pass

def reset(tower_map, iterator):
    tower_map.reset()
    iterator = tower_map.solve_hanoi(tower_map.num_disks())
    return iterator

def add_disk(tower_map, iterator):
    tower_map.reset()
    tower_map.add_disk()
    iterator = tower_map.solve_hanoi(tower_map.num_disks())
    return iterator

def remove_disk(tower_map, iterator):
    tower_map.reset()
    tower_map.remove_disk()
    iterator = tower_map.solve_hanoi(tower_map.num_disks())
    return iterator

class Disk(pygame.sprite.Sprite):
    def __init__(self, disk_id, n):
        pygame.sprite.Sprite.__init__(self)
        self._x = 0
        self._y = 0
        self._width = 0
        self._max = 96
        self._min = 32
        self._id = disk_id
        self._tower_index = 0
        self._create_image(n)

    def _create_image(self, n):
        diff = self._max - self._min
        diff = diff / (n-1)
        self._width = self._max - diff * self._id
        self.image = pygame.Surface([self._width, 12])
        self.rect = self.image.get_rect()
        pygame.draw.rect(self.image, (self.color_random(), self.color_random(), self.color_random()), self.rect)

    @staticmethod
    def color_random():
        return random.randint(0, 255)

    def set_tower_index(self, index):
        self._tower_index = index

    def move(self, x, y):
        self._x = x
        self._y = y
        self.rect.x = x
        self.rect.y = y

    def init_to_tower(self, index, height):
        self.set_tower_index(index)
        slide = 0
        if index == 0:
            slide = 48
        if index == 2:
            slide = -48
        self.move(816/4 * (index + 1) - self._width/2 + 6 + slide, 296 + 12 + height * -12)


class TowerMap(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self._tower1 = []
        self._tower2 = []
        self._tower3 = []
        self._n = 4
        self._disks = []
        self.create_disk()
        self._create_image()

    def num_disks(self):
        return len(self._disks)

    def add_disk(self):
        if self._n <= 8:
            self._n += 1
            disk = Disk(self._n - 1, self._n)
            self._disks.append(disk)
            self.reset()

    def remove_disk(self):
        if self._n > 2:
            self._n -= 1
            disk = self._disks.pop()
            disk.kill()
            self.reset()

    def create_disk(self):
        for i in range(self._n):
            disk = Disk(i, self._n)
            self._disks.append(disk)
            disk.init_to_tower(0, len(self._tower1))
            self._tower1.append(disk)

    def reset(self):
        self.clear_tower()
        for i in range(self._n):
            disk = self._disks[i]
            disk._create_image(self._n)
            disk.init_to_tower(0, len(self._tower1))
            self._tower1.append(disk)

    def get_disk(self, index):
        if index == 0:
            return self._tower1.pop()
        elif index == 1:
            return self._tower2.pop()
        else:
            return self._tower3.pop()

    def move_to(self, index1, index2):
        disk = self.get_disk(index1)
        self.add_disk_to(disk, index2)

    def disks(self):
        return self._disks

    def _create_image(self):
        self.image = pygame.Surface([816, 624], pygame.SRCALPHA, 32)
        self.rect = self.image.get_rect()
        pygame.draw.rect(self.image, (0, 0, 0), (816/4 + 48, 200, 10, 126))
        pygame.draw.rect(self.image, (0, 0, 0), (816/4 * 2, 200, 10, 126))
        pygame.draw.rect(self.image, (0, 0, 0), (816/4 * 3 - 48, 200, 10, 126))
        pygame.draw.rect(self.image, (0, 0, 0), (816/4 - 24, 320, 348 + 72 + 36, 12))

    def clear_tower(self):
        self._tower1 = []
        self._tower2 = []
        self._tower3 = []

    def index_pos(self, index):
        if index == 0:
            tower = self._tower1
        elif index == 1:
            tower = self._tower2
        else:
            tower = self._tower3
        return len(tower)

    def add_disk_to(self, disk, index):
        if index == 0:
            height = len(self._tower1)
            self._tower1.append(disk)
        elif index == 1:
            height = len(self._tower2)
            self._tower2.append(disk)
        else:
            height = len(self._tower3)
            self._tower3.append(disk)
        disk.init_to_tower(index, height)

    def solve_hanoi(self, n, a=0, b=1, c=2):
        if n == 0:
            yield
        if n > 0:
            yield from self.solve_hanoi(n - 1, a, c, b)
            self.move_to(a, c)
            yield from self.solve_hanoi(n - 1, b, a, c)

    def is_busy(self):
        return any([disk.is_busy() for disk in self._disks])


if __name__ == '__main__':
    main()