from random import randint

SIZE = 100


def randomDiff(n,maximum):
    rd_list = []
    while len(rd_list) != n:
        rd = randint(10,maximum)
        if not(rd in rd_list) and rd%SIZE == 0:
            rd_list.append(rd)
    return rd_list

def isCCW(start, end, pt):
    """Tests whether the turn formed by start, end, and pt is counterclock wise"""
    return (pt[0]-start[0]) * (end[1]-start[1]) > (pt[1]-start[1]) * (end[0]-start[0])


def leftPoints(start, end, pts):
    return [pt for pt in pts if isCCW(start,end,pt)]

def distance(start, end, pt): 
    a = end[1] - start[1]
    b = end[0] - start[0]
    c = (end[0] * start[1]) - (end[1] * start[0]) 
    return abs(a*pt[0] - b*pt[1] + c) / (a**2 + b**2)**0.5

def furthestPointFromLine(start, end, points):
    max_dist = 0
    max_point = None
    for point in points:
        if point != start and point != end:
            dist = distance(start, end, point)
            if dist > max_dist:
                max_dist = dist
                max_point = point
    return  max_point


def area(pt1,pt2,pt3):
    x1,y1 = pt1[0],pt1[1]
    x2,y2 = pt2[0],pt2[1]
    x3,y3 = pt3[0],pt3[1] 
    return abs(x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2))/2

def destruct(listPts,a,b,c):
    tri_area = area(a,b,c)
    outside = []
    inside = []
    for pt in listPts:
        a1 = area(a,pt,c)
        a2 = area(b,pt,c)
        a3 = area(a,pt,b)
        if (a1+a2+a3) == tri_area and pt != b: # car furthest serait enlevé des points car dans triangle
            inside.append(pt)
        elif not((a1+a2+a3) == tri_area) or not(pt != b):
            outside.append(pt)
    return inside,outside

def generatePoints(ptsNbr,maxX,maxY):
    """ generates random points """
    xPts = randomDiff(ptsNbr,maxX)
    yPts = randomDiff(ptsNbr,maxY)
    return [(x,y) for x,y in zip(xPts,yPts)]


class ConvexHull:


    def getExtremePoints(self,listPts):
        """ returns extreme points of the generated points"""
        pts = sorted(listPts,key=lambda x:x[0])
        return pts[0], pts[-1]

    def destruct(self,listPts,a,b,c):
        tri_area = area(a,b,c)
        outside = []
        inside = []
        for pt in listPts:
            a1 = area(a,pt,c)
            a2 = area(b,pt,c)
            a3 = area(a,pt,b)
            if (a1+a2+a3) == tri_area and pt != b: # car furthest serait enlevé des points car dans triangle
                inside.append(pt)
            else:
                outside.append(pt)
        return inside,outside

    def quickhull(self,listPts,minPt,maxPt):
        yield "link"
        yield minPt,maxPt
        leftOfLinePoints = leftPoints(minPt,maxPt,listPts)
        furthestPoint = furthestPointFromLine(minPt,maxPt,leftOfLinePoints)
        if not furthestPoint:
            yield from [maxPt]
        else:
            yield "link"
            yield minPt,furthestPoint
            yield "link"
            yield maxPt,furthestPoint
            yield "destruction"
            inside,leftOfLinePoints = self.destruct(leftOfLinePoints,minPt,furthestPoint,maxPt)
            yield inside
            yield from self.quickhull(leftOfLinePoints, minPt, furthestPoint)
            yield from self.quickhull(leftOfLinePoints,furthestPoint, maxPt)


    def findConvexHull(self,listPts):
        """ returns two generators that computes the convex hull """
        minPt,maxPt = self.getExtremePoints(listPts)
        return self.quickhull(listPts,minPt,maxPt),self.quickhull(listPts,maxPt,minPt)

