import kivy
kivy.require('1.10.1') 
from kivy.app import App
from animation import Animation
from menu import Menu
from kivy.uix.screenmanager import ScreenManager

class Handler:

	def __init__(self):
		self.manager = ScreenManager()
		menu = Menu(self.manager,name="menu")
		self.manager.add_widget(menu)
		

	def getManager(self):
		return self.manager



class MyApp(App):

    def build(self):
    	handler = Handler()
    	return handler.getManager()


if __name__ == '__main__':
    MyApp().run()