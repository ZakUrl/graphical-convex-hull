from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from animation import Animation
from kivy.app import App
from hanoi import main

class Menu(Screen):

	def __init__(self,manager,**kwargs):
		super(Menu,self).__init__(**kwargs)
		self.scManager = manager
		self.createLayout()

	def createLayout(self):
		self.layout = BoxLayout(orientation='horizontal')
		self.launchConv = Button(text="Enveloppe Convexe",on_press=self.launchConvex)
		self.launchHano = Button(text="Tour de Hanoi",on_press=self.launchHanoi)
		self.quitBut = Button(text="Quitter",on_press=self.quit)
		self.layout.add_widget(self.launchConv)
		self.layout.add_widget(self.launchHano)
		self.layout.add_widget(self.quitBut)
		self.add_widget(self.layout)


	def launchConvex(self,instance):
		self.anim = Animation(self.scManager,name="anim")
		self.anim.launch()
		self.scManager.add_widget(self.anim)
		self.scManager.current = 'anim'

	def launchHanoi(self,instance):
		main()


	def quit(self,instance):
		if App.get_running_app():
			App.get_running_app().stop()