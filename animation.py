from kivy.uix.screenmanager import Screen
from kivy.uix.widget import Widget
from kivy.graphics import *
from convex import ConvexHull, generatePoints
from planet import Planet
from kivy.clock import Clock

SPEED = 0.5

class Animation(Screen):

    def __init__(self,manager,**kwargs):
        super(Animation,self).__init__(**kwargs)
        self.scManager = manager
        self.bck = Rectangle(pos=self.pos,size=(1920,1080),source="./img/space.png")
        self.canvas.before.add(self.bck)
        self.canvas.ask_update()

    def launch(self):
        listPts = generatePoints(8,1850,900)
        self.generator1,self.generator2 = ConvexHull().findConvexHull(listPts)
        self.createPlanets(listPts)
        
        self.drawPlanets()
        self.event = Clock.schedule_interval(self.update,SPEED)

    def createPlanets(self,listPts):
        self.planets = []
        for pt in listPts:
            pl = Planet(pt)
            self.planets.append(pl)

    def drawPlanets(self):
        self.canvas.clear()
        for pl in self.planets:
            self.canvas.add(pl)

    def linkPlanets(self,pl1,pl2):
        self.canvas.add(Color(102/255, 41/255, 178/255))
        x1,y1 = pl1.getX()+Planet.SIZE/2,pl1.getY()+Planet.SIZE/2
        x2,y2 = pl2.getX()+Planet.SIZE/2,pl2.getY()+Planet.SIZE/2
        self.canvas.add(Line(points=(x1,y1,x2,y2),width=3))

    def findPlanet(self,coord):
        for pl in self.planets:
            if pl.pos == coord:
                return pl

    def update(self,*args):
        try:
            elem = next(self.generator1)
            if isinstance(elem,str):
                if elem == "link":
                    pl1,pl2 = next(self.generator1)
                    pl1 = self.findPlanet(pl1)
                    pl2 = self.findPlanet(pl2)
                    self.linkPlanets(pl1,pl2)
                elif elem == "destruction":
                    pls = next(self.generator1)
                    for pl in pls:
                        planet = self.findPlanet(pl)
                        planet.destroy()
            self.canvas.ask_update()
        except StopIteration as e:
            self.event.cancel()
            self.event1 = Clock.schedule_interval(self.update1,SPEED)

    def update1(self,*args):
        try:
            elem = next(self.generator2)
            if isinstance(elem,str):
                if elem == "link":
                    pl1,pl2 = next(self.generator2)
                    pl1 = self.findPlanet(pl1)
                    pl2 = self.findPlanet(pl2)
                    self.linkPlanets(pl1,pl2)
                elif elem == "destruction":
                    pls = next(self.generator2)
                    for pl in pls:
                        planet = self.findPlanet(pl)
                        planet.destroy()
            self.canvas.ask_update()
        except StopIteration as e:
            self.event1.cancel()




